/*-------------------------------------------------------------
 *
 * author: Dr Minsi Chen
 * e-mail. m.chen@hud.ac.uk
 *
 *------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "render.h"
#include "mathutils.h"

int main ( int argc, char** argv )
{
	//Declare 4 points
	Vector2 origin;
	Vector2 point1;
	Vector2 point2;
	
	Matrix2 rotation;
	
	//--Initialise the points
	vec2_set( &origin, 0, 0 );
	vec2_set( &point1, -10,20 );
	vec2_set( &point2, 15, 20);

	//--Set a rotation matrix
	mat2_setRotZ(&rotation, -6.0f);

	initbuffer();
	clearbuffer();
	
	//A simple rendering loop
	do
	{
		clearbuffer();

		drawline_Bresenham(&origin, &point1);
		drawline_Bresenham(&origin, &point2);
		drawline_Bresenham(&point1, &point2);

		point1 = mat2_mult_vec2(&rotation, &point1);
		point2 = mat2_mult_vec2(&rotation, &point2);

		drawpoint( &point1, '1' );
		drawpoint( &point2, '2' );
		
		displaybuffer();
	} while (getc(stdin) != 'q');

	return 0;
}
