/*-------------------------------------------------------------
 *
 * author: Dr Minsi Chen
 * e-mail. m.chen@hud.ac.uk
 *
 *------------------------------------------------------------*/

#ifndef __RENDER_H__
#define __RENDER_H__

#include "mathutils.h"

#if 0
#define BUFFER_X	80
#define BUFFER_Y	80
#define BUFFER_X_PADDED BUFFER_X + 1
#define BUFFER_SIZE	(BUFFER_X_PADDED*BUFFER_Y)+1
#endif

void clearbuffer();
void initbuffer();
void displaybuffer();
void drawpoint( const Vector2* pt, char c );

char getpixel( const Vector2* pt);

void drawline_Bresenham(const Vector2* pt1, const Vector2* pt2);

void floodfill( int x, int y, char c);

#endif
