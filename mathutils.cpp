/*-------------------------------------------------------------
 *
 * author: Dr Minsi Chen
 * e-mail. m.chen@hud.ac.uk
 *
 *------------------------------------------------------------*/

#include <stdlib.h>
#include <math.h>

#include "mathutils.h"

#define PI 3.1415926535897932384626433832795

void vec2_set( Vector2* vec, int x, int y )
{
	vec->x = x;
	vec->y = y;
}

void mat2_setRotZ( Matrix2* mat, float degree )
{
	float radian = degree*PI/180.0f;
	mat->m[0][0] = cos(radian); mat->m[0][1] = -sin(radian);
	mat->m[1][0] = sin(radian); mat->m[1][1] = cos(radian);
}

Vector2 mat2_mult_vec2( const Matrix2* mat, const Vector2* vec )
{
	Vector2 result;

	result.x = floor( mat->m[0][0]*vec->x + mat->m[0][1]*vec->y );
	result.y = floor( mat->m[1][0]*vec->x + mat->m[1][1]*vec->y );
	
	return result;
}
