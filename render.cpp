/*-------------------------------------------------------------
 *
 * author: Dr Minsi Chen
 * e-mail. m.chen@hud.ac.uk
 *
 *------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>

#include "render.h"

static const int  BUFFER_X = 100;
static const int  BUFFER_Y = 50;
static const int  BUFFER_X_PADDED = BUFFER_X + 1;
static const int  BUFFER_SIZE = (BUFFER_X_PADDED*BUFFER_Y);
static const int ORIGIN_X = BUFFER_X >> 1;
static const int ORIGIN_Y = -(BUFFER_Y >> 1);

static char s_framebuffer[BUFFER_SIZE+1];

void initbuffer ()
{
	int y = 0;

	memset(s_framebuffer, 0x20, BUFFER_SIZE*sizeof(char));
	s_framebuffer[BUFFER_SIZE-1] = 0x00;

	for ( ; y < BUFFER_Y; y++ )
	{
		s_framebuffer[BUFFER_X+y*BUFFER_X_PADDED] = 0x0a;
	}

}

void clearbuffer()
{
	int mx = BUFFER_X >> 1;
	int my = BUFFER_Y >> 1;

	initbuffer();

	for ( int x = 0;  x < BUFFER_X; x++ )
	{
		s_framebuffer[x+my*BUFFER_X_PADDED] = 0x2d;
	}

	for ( int y = 0; y < BUFFER_Y; y++ )
	{
		s_framebuffer[mx + y*BUFFER_X_PADDED] = 0x7c;
	}
	
	s_framebuffer[mx+my*BUFFER_X_PADDED] = 0x2b;
	s_framebuffer[mx] = 0x59;
	s_framebuffer[BUFFER_X-1+my*BUFFER_X_PADDED] = 0x58;
}

void displaybuffer()
{
	//First clear the cmd line terminal
	system("cls");

	//Draw the entire framebuffer content.
	printf("%s\b", s_framebuffer);
}

void drawpoint( const Vector2* pt, char c )
{
	int buff_idx = (pt->x + ORIGIN_X) + ((BUFFER_Y - pt->y) + ORIGIN_Y)*BUFFER_X_PADDED;	

	if ( buff_idx >= 0 && buff_idx < BUFFER_SIZE )
		s_framebuffer[buff_idx] = c;
}

char getpixel( const Vector2* pt )
{
	int buff_idx = (pt->x + ORIGIN_X) + ((BUFFER_Y - pt->y) + ORIGIN_Y)*BUFFER_X_PADDED;
	char output = '\0';

	if ( buff_idx >= 0 && buff_idx < BUFFER_SIZE )
		output = s_framebuffer[buff_idx];

	return output;
}

void drawline_Bresenham(const Vector2* pt1, const Vector2* pt2)
{
	bool swap_x = pt2->x < pt1->x;

	int dx = swap_x?pt1->x - pt2->x:pt2->x - pt1->x;
	int dy = swap_x?pt1->y - pt2->y:pt2->y - pt1->y;

	int reflect = dy < 0 ? -1 : 1;
	bool swap_xy = dy*reflect > dx;
	int epsilon = 0;
	
	int sx = swap_xy ? reflect < 0 ? swap_x ? pt1->y : pt2->y : swap_x ? pt2->y : pt1->y : swap_x ? pt2->x : pt1->x;
	int y = swap_xy ? reflect < 0 ? swap_x ? pt1->x : pt2->x : swap_x ? pt2->x : pt1->x : swap_x ? pt2->y : pt1->y;
	int ex = swap_xy ? reflect < 0 ? swap_x ? pt2->y : pt1->y : swap_x ? pt1->y : pt2->y : swap_x ? pt1->x : pt2->x;
	
	int x = sx;
	y *= reflect;

	Vector2 temp;

	while (x <= ex)
	{
		temp.x = swap_xy ? y*reflect : x;
		temp.y = swap_xy ? x : y*reflect;

		drawpoint(&temp, 'o');
		
		epsilon += swap_xy? dx : dy*reflect;

		if ( (epsilon << 1) >= (swap_xy ? dy*reflect : dx) )
		{
			y++;
			
			epsilon -= swap_xy ? dy*reflect : dx;
		}
		x++;
	}
}

void floodfill(int x, int y,  char c)
{
	Vector2 pt;
	pt.x = x;
	pt.y = y;

	char pixel = getpixel(&pt);
	
	if ( pixel != c )
	{
		drawpoint ( &pt, c );

		floodfill( x-1, y, c);
		floodfill( x+1, y, c);
		floodfill( x, y-1, c);
		floodfill( x, y+1, c);
	}
}
