/*-------------------------------------------------------------
 *
 * author: Dr Minsi Chen
 * e-mail. m.chen@hud.ac.uk
 *
 *------------------------------------------------------------*/

#ifndef __LAUTILS_H__
#define __LAUTILS_H__

typedef struct
{
	int x,y;
} Vector2;

typedef struct
{
	float m[2][2];
} Matrix2;

void vec2_set( Vector2* vec, int x, int y );

void mat2_setRotZ( Matrix2* mat, float degree );

Vector2 mat2_mult_vec2( const Matrix2* mat,  const Vector2* vec );
#endif
